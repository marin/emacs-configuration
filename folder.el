;;;; folder.el: fold regions of text -*- lexical-binding: t; -*-

;;;; Fold and unfold regions of text. GPL license. 

(defface folder-overlay-extra-text-face
  '((t (:inherit nil
                 :foreground "red"
                 :italic t
                 :bold t)))
  "Face used for the prefix displayed for folded regions")

(defface folder-overlay-text-face
  '((t (:inherit nil :italic t :background "dark green")))
  "face to add to text in an overlay")

(defvar folder-overlay-prefix ""
  "Text to prepend to a folded overlay.")

(defvar folder-overlay-middle
  (propertize " ... " 'face 'folder-overlay-extra-text-face)
  "Text to place between the begin and end strings of a folder overlay.")

(defvar folder-overlay-suffix ""
  "Text to append to a folded overlay.")

(defvar folder-beginning-of-text-length 10
  "The length of the string for the beginning of a folder overlay.")

(defvar folder-end-of-text-length 10
  "The length of the string for the end of a folder overlay.")

(defvar folder-calculate-fold-region-text-function
  'folder--calculate-fold-region-text
  "Function to calculate and return the text to display in a folder
overlay. This function will be called with a beginning buffer
position and an end buffer position and must return a string")

(defun folder-get-line (pos)
  "Get a single line with text properties"
  (let (beg end)
    (save-excursion 
      (goto-char pos)
      (beginning-of-line)
      (setq beg (point))
      (end-of-line)
      (setq end (point)))
    (buffer-substring beg end)))

(defun folder--calculate-fold-region-text (beg end)
  (let* ((beg-text
          (let ((line (folder-get-line beg)))
            (if (< (- (length line) 1) folder-beginning-of-text-length)
                line
              (substring line 0 folder-beginning-of-text-length))))
         (end-text
          (let ((line (folder-get-line end)))
            (if (< (- (length line) 1) folder-end-of-text-length)
                line
              (substring line (- (length line) folder-end-of-text-length)
                         (length line)))))
         (final-text (concat folder-overlay-prefix
                             beg-text
                             folder-overlay-middle
                             end-text
                             folder-overlay-suffix)))
    (add-face-text-property 0 (length final-text)
                            'folder-overlay-text-face t final-text)
    final-text))

(defun folder-overlay-put-specs (overlay specs)
  (mapc (lambda (spec) (overlay-put overlay (car spec) (cadr spec))) specs))

(defun folder-fold-region (beg end)
  (interactive "r")
  (when (region-active-p)
    (let ((overlay-text
           (funcall folder-calculate-fold-region-text-function beg end))
          (overlay (make-overlay beg end nil t nil)))
      (folder-overlay-put-specs
       overlay
       (list '(invisible t)
             '(type folder-region-overlay)
             '(isearch-open-invisible folder--remove-overlay)
             '(modification-hooks (folder--remove-overlay))
             (list 'display overlay-text)
             '(evaporate t)))
      (overlay-put overlay
                   'isearch-open-invisible-temporary
                   (lambda (overlay make-invisible)
                     (folder-overlay-put-specs
                      overlay
                      (if make-invisible
                          (list (list 'display overlay-text)
                                (list invisible t))
                        (list (list 'display nil) (list 'invisible nil)))))))
    (deactivate-mark)))

(defun folder-overlay-type-p (overlay)
  (eq (overlay-get overlay 'type) 'folder-region-overlay))

(defun folder--remove-overlay (overlay &rest rest)
  (when (folder-overlay-type-p overlay)
    (delete-overlay overlay)))

(defun folder-unfold-overlays (overlays)
  (mapc 'folder--remove-overlay overlays))

(defun folder-unfold-point ()
  "Unfold the folder overlays at point."
  (interactive)
  (folder-unfold-overlays (overlays-at (point))))

(defun folder-unfold-region (beg end)
  "Unfold the folder overlays within the region."
  (interactive "r")
  (folder-unfold-overlays (overlays-in beg end)))

(defun folder-unfold-buffer ()
  "Unfold all folder overlays within the current buffer."
  (interactive)
  (folder-unfold-region (point-min) (point-max)))

(defun folder-unfold ()
  (interactive)
  (cond ((and current-prefix-arg (= current-prefix-arg 4)) (folder-unfold-buffer))
        ((region-active-p) (folder-unfold-region (region-beginning) (region-end)))
        (t (folder-unfold-point))))

(defun folder-toggle-fold ()
  (interactive)
  (cond (current-prefix-arg
         (cond ((= (car current-prefix-arg) 4)
                (if (region-active-p)
                    (folder-unfold-region (region-beginning) (region-end))
                  (message "No region currently active")))
               ((= (car current-prefix-arg) 16)
                (folder-unfold-buffer)))
         (deactivate-mark))
        ((region-active-p)
         (folder-fold-region (region-beginning) (region-end)))
        ((cl-member-if #'folder-overlay-type-p (overlays-at (point)))
         (folder-unfold))
        (t
         (message "No folder overlay at point and no active region to fold"))))

(provide 'folder)
