;;;; oh-shit-my-window-config.el: save window config -*- lexical-binding: t; -*-

;;;; Save the window configuration before the current window configuration. This
;;;; is useful for those "oh shit thats not right" moments, where you run a
;;;; command that changes the window configuration without saving the
;;;; configuration with { M-x window-configuration-to-register RET }. This is
;;;; done by saving the window configuration before every command (unless it is
;;;; a config saving command). Then the user can explicitly save the previous
;;;; window config to a special variable used to restore the config (or to a
;;;; register).

(defvar osmwc--window-configuration-register nil)
(defvar osmwc--most-recent-window-configuration nil)

(defun osmwc-register-current-window-configuration ()
  (setq osmwc--most-recent-window-configuration (current-window-configuration)))

(defun osmwc-save-previous-window-config ()
  (interactive)
  (when osmwc--most-recent-window-configuration
    (push osmwc--most-recent-window-configuration
          osmwc--window-configuration-register)))

(defun osmwc-restore-previous-window-config ()
  (interactive)
  (if osmwc--window-configuration-register
      (progn (set-window-configuration (car osmwc--window-configuration-register))
             (unless current-prefix-arg
               (pop osmwc--window-configuration-register)))
    (message "No window configuration available to restore")))

(defun osmwc-before-command-save-config ()
  (unless (eql this-command 'osmwc-save-previous-window-config)
    (osmwc-register-current-window-configuration)))

(defun enable-osmwc (arg)
  (if arg
      (add-hook  'pre-command-hook 'osmwc-before-command-save-configuration)
    (remove-hook 'pre-command-hook 'osmwc-before-command-save-configuration)))

(provide 'oh-shit-my-window-config)
